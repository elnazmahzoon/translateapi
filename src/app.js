import React from 'react';
import ReactDOM from 'react-dom';
import Translate from './components/Translate';
import 'normalize.css/normalize.css';
import './styles/styles.scss';

ReactDOM.render(<Translate />, document.getElementById('app'));
