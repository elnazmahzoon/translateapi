import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLanguage } from '@fortawesome/free-solid-svg-icons';

const Header = (props) => (
  <div className="header">
    <div className="container align-items-center">
      <FontAwesomeIcon icon={faLanguage} size="2x"></FontAwesomeIcon>
      <h1 className="header__title">{props.title}</h1>
      {props.subtitle && <h2 className="header__subtitle">{props.subtitle}</h2>}
    </div>
  </div>
);

Header.defaultProps = {
  title: 'Translate'
};

export default Header;
