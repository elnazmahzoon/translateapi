import React from 'react';
import Header from './Header';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { setCORS } from "google-translate-api-browser";

export default class Translate extends React.Component {
  constructor(props) {
    super(props);
    this.translateFunction = this.translateFunction.bind(this);
    this.state = {
      result: ""
    };
  }
  translateFunction(e) {
    const translate = setCORS("http://cors-anywhere.herokuapp.com/");
    translate(e.target.value, { to: "fa" })
      .then(res => {
        this.setState(() => ({ result: res.text }));
      })
      .catch(err => {
        console.error(err);
      });
  }

  render() {
    const subtitle = 'English to Persian';
    return (
      <div>
        <Header subtitle={subtitle} />
        <div className="container">
          <form className="d-flex justify-content-between">
            <div>
              <p className="font-weight-bold">Enter your text in Enlgish</p>
              <textarea className="widget" type="text" name="inputText" onChange={this.translateFunction}></textarea>
            </div>
            <FontAwesomeIcon className="arrowStyle" icon={faArrowRight} size="2x"></FontAwesomeIcon>
            <div>
              <p className="font-weight-bold">Translated text in Persian</p>
              <textarea className="widget" type="text" name="outputText" value={this.state.result} ></textarea>
            </div>
          </form>
        </div>
      </div>
    );
  }
}