# TranslateAPI Project

## Install packages

In order to install TranslateAPI's packages, you need to do the following steps:
1. Download and install Nodejs from https://nodejs.org/en/ inorder to use npm.
2. Install yarn with `npm install -g yarn` command.
3. Open terminal in project's folder.
4. Run `yarn install` command in your terminal. This command will install all mandatory packages for this project.

## Run project
In order to run the TranslateAPI project on your local host, you can use dev server (the following command): 

```dev-server
yarn run dev-server
```

This command will build the project and also open it on your localhost:8080 (check the command result on the terminal to see if the port was different to 8080)

## Notice

You can only send 100 requests per hour, because this project uses free API.  :)

